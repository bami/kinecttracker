/*
 * coordinate_transform.cpp
 *  Copyright (C) 2011 Christoph Bubenhofer
 *
 *  This file is part of SkeletonTracker.
 *
 *  SkeletonTracker is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SkeletonTracker is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with OpenNI. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "coordinate_transform.h"

int BM_SMOOTH_ACC = 10; //for smoothing the accelermoter data

// --- PRIVATE HEADER ---
// A[3x3] * B[3x3] = C[3x3]
void __ST_multiplicationMatrix3x3Matrix3x3(double A[], double B[], double C[]);

// A[3x3] * b[3] = c[3]
void __ST_multiplicationMatrix3x3Vector3x1(double A[], double b[], double c[]);


// --- SOURCE CODE ---
//matrix multiplication of two 3x3 matrixes
void __ST_multiplicationMatrix3x3Matrix3x3(double A[], double B[], double C[])
{
	for (int row = 0; row < 3; row++) {
		for (int columne = 0; columne < 3; columne++) {
			C[3 * row + columne] = A[row * 3] * B[columne] + A[row * 3 + 1]
					* B[columne + 3] + A[row * 3 + 2] * B[columne + 6]; // 3 due to the fact that it is a 3x3 matrix
		}
	}
}

//for 3x3 matrix saved in an array double[9] and a vector with 3 rows
void __ST_multiplicationMatrix3x3Vector3x1(double A[], double b[], double c[])
{
	for (int row = 0; row < 3; ++row)
	{
		c[row] = A[row * 3] * b[0] + A[row * 3 + 1] * b[1] + A[row * 3 + 2]
				* b[2]; // 3 due to the fact that it is a 3x3 matrix
	}
}

XnVector3D ST_cooridnateTransform(XnVector3D position) {
	double distanceKinTo0X = 0; //distance to from Kinect to 0 in mm
	double distanceKinTo0Y = 0;
	double distanceKinTo0Z = 0;

	double alpha, beta, gamma;
	XnVector3D g;
	int horizontalAngle = 0;
	double delta = (double) horizontalAngle / 180 * M_PI;
	double neigung = 0;

	//	printf("\nDie Winkel alpha und alpha: %f\n",delta/M_PI*180);

	double cosAlpha, sinAlpha, cosBeta, sinBeta, cosDelta, sinDelta,
			cosNeigung, sinNeigung;

	//delta: angles to turn around y system of earth in rad
	BM_KinectAux_getOrientation(&alpha,&beta,&gamma);

	cosAlpha = cos(alpha);
	sinAlpha = sin(alpha);

	cosBeta = cos(beta);
	sinBeta = sin(beta);

	cosDelta = cos(delta);
	sinDelta = sin(delta);

	cosNeigung = cos(neigung);
	sinNeigung = sin(neigung);

	//rotation matrixes around x,y,z
	double rotationX[9] = { 1,		0,			0,
							0,		cosBeta,	sinBeta,
							0,		-sinBeta,	cosBeta };

	double rotationZ[9] = {	cosAlpha,		sinAlpha,		0,
							-sinAlpha,		cosAlpha,		0,
							0,				0,				1 };

	double rotationY[9] = {	cosDelta,		0,		-sinDelta,
							0,				1,		0,
							sinDelta,		0,		cosDelta };

	// calculate ratation matrix for three dimensions in rotating order Z,X,Y
	double rotationXZ[9];
	double rotationYXZ[9];
	__ST_multiplicationMatrix3x3Matrix3x3(rotationX, rotationZ, rotationXZ);
	__ST_multiplicationMatrix3x3Matrix3x3(rotationY, rotationXZ, rotationYXZ);

	double positionVector[3];
	positionVector[0] = position.X;
	positionVector[1] = position.Y;
	positionVector[2] = position.Z;

	double correctedPositionVector[3]; //multiplying Vector with TransformMatrix

	__ST_multiplicationMatrix3x3Vector3x1(rotationYXZ, positionVector,
			correctedPositionVector);

	correctedPositionVector[0] = correctedPositionVector[0] - distanceKinTo0X; //correcting with the distance from Kinect to 0
	correctedPositionVector[1] = correctedPositionVector[1] - distanceKinTo0Y;
	correctedPositionVector[2] = correctedPositionVector[2] - distanceKinTo0Z;

	//rotation matrixes around x,y,z
	double rotationN[9] = {	1,	0,				0,
							0,	cosNeigung, 	sinNeigung,
							0,	-sinNeigung,	cosNeigung };

	double correctedPositionVectorNeigung[3];

	__ST_multiplicationMatrix3x3Vector3x1(rotationN, correctedPositionVector,
			correctedPositionVectorNeigung);

	XnVector3D correctedPosition; //saving into return value

	correctedPosition.X = correctedPositionVectorNeigung[0];
	correctedPosition.Y = correctedPositionVectorNeigung[1];
	correctedPosition.Z = correctedPositionVectorNeigung[2];

	return correctedPosition;
}

// translate vector
void ST_setOrigin(XnVector3D *position, XnVector3D origin)
{
	//newposition = position - origin
	position->X = position->X - origin.X;
	position->Y = position->Y - origin.Y;
	position->Z = position->Z - origin.Z;
}

void ST_setEarthOrientation(XnVector3D *position)
{
	throw "not implemented!";
}

// rotate around delta (y), alpha (z), beta (x) [RAD]
void ST_setRotation(XnVector3D *position, double alpha, double beta,
					double delta)
{
	double positionVector[3] = { position->X, position->Y, position->Z };
	double correctedPositionVector[3];
	double cosAlpha, sinAlpha, cosBeta, sinBeta, cosDelta, sinDelta;

	cosAlpha = cos(alpha);
	sinAlpha = sin(alpha);

	cosBeta = cos(beta);
	sinBeta = sin(beta);

	cosDelta = cos(delta);
	sinDelta = sin(delta);

	//rotation matrixes around x,y,z
	double rotationX[9] = { 1, 	0, 			0,
							0,	cosBeta,	sinBeta,
							0,	-sinBeta,	cosBeta };

	double rotationZ[9] = { cosAlpha,	sinAlpha,	0,
							-sinAlpha,	cosAlpha,	0,
							0,			0,			1 };

	double rotationY[9] = { cosDelta,	0,	-sinDelta,
							0,			1,	0,
							sinDelta, 	0,	cosDelta };

	// calculate ratation matrix for three dimensions in rotating order Z,X,Y
	double rotationXZ[9];
	double rotationYXZ[9];
	__ST_multiplicationMatrix3x3Matrix3x3(rotationX, rotationZ, rotationXZ);
	__ST_multiplicationMatrix3x3Matrix3x3(rotationY, rotationXZ, rotationYXZ);

	__ST_multiplicationMatrix3x3Vector3x1(rotationYXZ, positionVector,
			(double*)&correctedPositionVector);

	position->X = correctedPositionVector[0];
	position->Y = correctedPositionVector[1];
	position->Z = correctedPositionVector[2];
}

// scale vector
void ST_setSkale(XnVector3D *position, double scalingFactor)
{
	position->X *= scalingFactor;
	position->Y *= scalingFactor;
	position->Z *= scalingFactor;
}

