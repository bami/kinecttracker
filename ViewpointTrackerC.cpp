#include "ViewpointTracker.h"

// C function for ctypes python interface

extern "C"
{
ViewpointTracker* ViewpointTracker_instance(){
return ViewpointTracker::Instance();
}

void ViewpointTracker_setSmoothing(ViewpointTracker* tracker, double alpha) {
	tracker->setSmoothingFactor(alpha);
}

void ViewpointTracker_getSmoothing(ViewpointTracker* tracker, double* alpha) {
	*alpha = tracker->getSmoothingFactor();
}

void ViewpointTracker_setKinectTiltAngle(ViewpointTracker* tracker, int angle) {
	tracker->setKinectTiltAngle(angle);
}

void ViewpointTracker_getKinectOrientation(ViewpointTracker* tracker,
		double *roll_z, double *pitch_x, double *acc_y) {
	tracker->getKinectOrientation(roll_z, pitch_x, acc_y);
}

void ViewpointTracker_wipeRepository(ViewpointTracker* tracker) {
	tracker->wipeRepository();
}

void ViewpointTracker_printRepository(ViewpointTracker* tracker) {
	tracker->printRepository();
}
int ViewpointTracker_getViewpoint(ViewpointTracker* tracker, int id, double *x,
		double *y, double *z) {
	if (id > 24)
		return -1;

	XnVector3D vps[24];
	tracker->getViewpoints((XnVector3D*) &vps, 24);
	*x = (double) vps[id].X;
	*y = (double) vps[id].Y;
	*z = (double) vps[id].Z;
	return 0;
}

int ViewpointTracker_getNumerOfUsers(ViewpointTracker* tracker){
	return tracker->getNUsers();
}

}
