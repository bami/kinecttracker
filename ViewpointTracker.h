/*
 * ViewpointTracker
 *
 *  Created on: Nov 29, 2011
 *      Author: miggeb@ethz.ch
 *
 *  the viewpoint is defined as 3d point VIEWPOINT_HEADTOP_OFFSET measures
 *  below the highest point of the identified user
 *
 *  3D coordinates:
 *  z = distance to camera
 *  x = horizontal camera coordinates
 *  y = vertical camera coordinates
 *
 *  based on
 *  PrimeSense NITE 1.3 - Players Sample
 *  (Copyright (C) 2010 PrimeSense Ltd.)
 */


#ifndef VIEWPOINTTRACKER_H_
#define VIEWPOINTTRACKER_H_

#include <string>
#include <pthread.h>
#include <XnCppWrapper.h>
#include <XnOpenNI.h>

#include "coordinate_transform.h"
extern "C"
{
	#include "kinect_aux.h"
}

// ----------------
// Defines
// ----------------

#define VIEWPOINT_HEADTOP_OFFSET 150 // 15 cm beneath top of head
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))

#define CHECK_RC(rc, what)											\
	if (rc != XN_STATUS_OK)											\
	{																\
		printf("%s failed: %s\n", what, xnGetStatusString(rc));		\
		return rc;													\
	}

#define CHECK_ERRORS(rc, errors, what)		\
	if (rc == XN_STATUS_NO_NODE_PRESENT)	\
{										\
	XnChar strError[1024];				\
	errors.ToString(strError, 1024);	\
	printf("%s\n", strError);			\
	return (rc);						\
}

#define XML_CONFIG_PATH  "./Config/Config.xml"

// ----------------
// Class ViewpointTracker
// tracks the viewpoint of users with MS Kinect
// Coordinate System (positive direction): X sideways, Y upward, Z distance
// ----------------
class ViewpointTracker
{
private:

	static const XnUInt16 N_JOINTS = 24;
	static const XnUInt16 N_USERS = 15;

	int nUsers; // number of detected users

	XnVector3D viewpoint_repos[N_USERS];
	pthread_mutex_t repos_mutex;
	pthread_t updateThread;

	xn::Context g_Context;
	xn::ScriptNode g_ScriptNode;
	xn::DepthGenerator g_DepthGenerator;
	xn::UserGenerator g_UserGenerator;

	XnUserID g_nPlayer;
	XnBool g_bCalibrated;

	XnStatus nRetVal;
	XnBool bPause;

	double smoothingFactor; // alpha value for exponential smoothing

	ViewpointTracker(); // init
	ViewpointTracker(ViewpointTracker const&){}; // copy
	//SkeletonTracker& operator=(SkeletonTracker const&){}; // assignment
	static ViewpointTracker* m_pInstance;


	// kinect control
	int initKinect();
	int shutdownKinect();
    int refreshKinect();

    // user tracker
	void FindPlayer();
	void LostPlayer();
	XnBool AssignPlayer(XnUserID user);


    // repository
    void initRepository();
    void updateRepository(const xn::DepthMetaData& depthMD,
    		const xn::SceneMetaData& sceneMD);

    // thread update callback
    	static void* updateLoop(void* args); // thread loop


public:
    static ViewpointTracker* Instance();
	~ViewpointTracker();

	// kinect control
	void setKinectTiltAngle(int angle);
	// spherical coordinates around z,x,y axis (y is invalid, since we use a g-sensor)
	void getKinectOrientation(double *roll_z, double *pitch_x, double *acc_y);

	// repository handler
	void wipeRepository();

	// print repository to STDOUT
	void printRepository();

	// return viewpoints in camera coordinate system
	int getViewpoints(XnVector3D* viewpointDestination, int n_viewpoints);

	// return the number of detected users
	int getNUsers();

	// set smoothing parameter [0,1]
	int setSmoothingFactor(double alpha);
	// get smoothing parameter [0,1]
	double getSmoothingFactor();

	// openNI callbacks
	static void XN_CALLBACK_TYPE NewUser(xn::UserGenerator& generator, XnUserID user, void* pCookie);
	static void XN_CALLBACK_TYPE LostUser(xn::UserGenerator& generator, XnUserID user, void* pCookie);



};

#endif /* SKELETONTRACKER_H_ */
