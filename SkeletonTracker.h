/*
 * SkeletonTracker.h
 *
 *  Created on: Nov 21, 2011
 *      Author: bami
 *
 *  SkeletonTracker
 *
 *  based on
 *  PrimeSense NITE 1.3 - Players Sample
 *  (Copyright (C) 2010 PrimeSense Ltd.)
 */


#ifndef SKELETONTRACKER_H_
#define SKELETONTRACKER_H_

#include <string>
#include <XnCppWrapper.h>
#include <XnOpenNI.h>


#include <pthread.h>

extern "C"
{
	#include "kinect_aux.h"
}

// ----------------
// Defines
// ----------------
#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))

#define CHECK_RC(rc, what)											\
	if (rc != XN_STATUS_OK)											\
	{																\
		printf("%s failed: %s\n", what, xnGetStatusString(rc));		\
		return rc;													\
	}

#define CHECK_ERRORS(rc, errors, what)		\
	if (rc == XN_STATUS_NO_NODE_PRESENT)	\
{										\
	XnChar strError[1024];				\
	errors.ToString(strError, 1024);	\
	printf("%s\n", strError);			\
	return (rc);						\
}

#define XML_CONFIG_PATH  "./Config/Config.xml"

// ----------------
// Class
// ----------------
class SkeletonTracker
{
private:

	static const XnUInt16 N_JOINTS = 24;
	static const XnUInt16 N_USERS = 15;

	XnSkeletonJointPosition skeletons_repos[N_USERS][N_JOINTS];
	XnUserID skeletons_id[N_USERS];
	int n_skeleton_repos;
	pthread_mutex_t repos_mutex;
	pthread_t updateThread;

	xn::Context g_Context;
	xn::ScriptNode g_ScriptNode;
	xn::DepthGenerator g_DepthGenerator;
	xn::UserGenerator g_UserGenerator;

	XnUserID g_nPlayer;
	XnBool g_bCalibrated;

	XnStatus nRetVal;
	XnBool bPause;

	SkeletonTracker(); // init
	SkeletonTracker(SkeletonTracker const&){}; // copy
	//SkeletonTracker& operator=(SkeletonTracker const&){}; // assignment
	static SkeletonTracker* m_pInstance;


	// kinect control
	XnStatus initKinect();
	void shutdownKinect();
    void refreshKinect();

    // user tracker
	void FindPlayer();
	void LostPlayer();
	XnBool AssignPlayer(XnUserID user);

    // repository
    void updateRepository();
    static void* updateLoop(void* args); // thread loop

    void extractHeadPositionsFromDepthCam(const xn::DepthMetaData& depthMD,
    		const xn::SceneMetaData& sceneMD);

public:
    static SkeletonTracker* Instance();
	~SkeletonTracker();

	// kinect control
	void setKinectTiltAngle(int angle);
	// get the specical coordinates of the device
	void getKinectOrientation(double *a,double *b, double *c);
	void saveCalibration(std::string filename);
	void loadCalibration(std::string filename);

	// repository handler
	void wipeRepository();
	void printRepository();
	// get skeletons in camera coordinate system
	int getSkeletons(XnSkeletonJointPosition* skeletonsDestination, int n_Skeletons);

	// openNI callbacks
	static void XN_CALLBACK_TYPE NewUser(xn::UserGenerator& generator, XnUserID user, void* pCookie);
	static void XN_CALLBACK_TYPE LostUser(xn::UserGenerator& generator, XnUserID user, void* pCookie);
	static void XN_CALLBACK_TYPE PoseDetected(xn::PoseDetectionCapability& pose, const XnChar* strPose, XnUserID user, void* cxt);
	static void XN_CALLBACK_TYPE CalibrationStarted(xn::SkeletonCapability& skeleton, XnUserID user, void* cxt);
	static void XN_CALLBACK_TYPE CalibrationEnded(xn::SkeletonCapability& skeleton, XnUserID user, XnBool bSuccess, void* cxt);
	static void XN_CALLBACK_TYPE CalibrationCompleted(xn::SkeletonCapability& skeleton, XnUserID user, XnCalibrationStatus eStatus, void* cxt);

};

#endif /* SKELETONTRACKER_H_ */
