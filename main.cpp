//============================================================================
// Name        : SkeletonTrackerKinectCommandLine.cpp
// Author      : Bastian Migge
// Version     : 0.1
// Copyright   : GPL
// Description : Command line interface for kinect 3D skeleton tracker
//============================================================================

#include "SkeletonTracker.h"
#include "ViewpointTracker.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* usleep */

#define UPDATE_RATE 0.1 // [Hz]

void runSkeletonTracker()
{
	SkeletonTracker* viewpointTracker;
	XnVector3D vp;
	int numberOfSkeletons;

	// user tracker
	viewpointTracker = SkeletonTracker::Instance();
	viewpointTracker->setKinectTiltAngle(0);
	XnSkeletonJointPosition skeleton[24];

	while (1) {

		// get user position (first user)
		numberOfSkeletons = viewpointTracker->getSkeletons(
						(XnSkeletonJointPosition*) &skeleton, 1);


		if (numberOfSkeletons < 1)
			continue;

		vp = skeleton[XN_SKEL_HEAD].position;
		printf("%f,%f,%f\n",vp.X,vp.Y,vp.Z);
		usleep((int) (1000 / UPDATE_RATE));
	}
}

void runViewpointTracker()
{

	ViewpointTracker* viewpointTracker;
	XnVector3D vp;
	int numberOfViewpoints;

	// user tracker
	viewpointTracker = ViewpointTracker::Instance();
	viewpointTracker->setKinectTiltAngle(10);
	XnVector3D vps[24];

	double a,b,c;
	while (1)
	{
		viewpointTracker->printRepository();
		printf("\n");
		viewpointTracker->getKinectOrientation(&a,&b,&c);
		printf("orientation: %f,%f,%f\n",a,b,c);
		sleep(1);
	}
}


int main(int argc, char **argv)
{
	runViewpointTracker();
	runSkeletonTracker();
}
