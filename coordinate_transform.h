/*
 * coordinate_transform.h
*  Copyright (C) 2011 Christoph Bubenhofer
*
*  This file is part of SkeletonTracker.
*
*  SkeletonTracker is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Lesser General Public License as published
*  by the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  SkeletonTracker is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
*  GNU Lesser General Public License for more details.
*
*  You should have received a copy of the GNU Lesser General Public License
*  along with OpenNI. If not, see <http://www.gnu.org/licenses/>.
*
*****************************************************************************/

#ifndef COORDINATE_TRANSFORM_H_
#define COORDINATE_TRANSFORM_H_

#include <math.h>
#include <XnTypes.h>
extern "C"
{
	#include "kinect_aux.h"
}

// rotate to earth coordinate system using the Kinect accelerometer
void ST_setEarthOrientation(XnVector3D *position);

//to transform the coordinate system
XnVector3D ST_cooridnateTransform(XnVector3D position);

// translations
void ST_setOrigin(XnVector3D *position, XnVector3D origin);

void ST_setRotation(XnVector3D *position, double alpha, double beta,
					double delta);

// scaling
void ST_setSkale(XnVector3D *position, double scalingFactor);
#endif /* COORDINATE_TRANSFORM_H_ */
