/*
 * kinect_aux.h
 *
 *  Created on: May 13, 2011
 *  Author: Bastian migge <miggeb@ethz.ch>
 *
 *  Based on kinect_aux http://www.ros.org/wiki/kinect
 */
#ifndef _KINECT_AUX_
#define _KINECT_AUX_

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <libusb-1.0/libusb.h>
#include <XnTypes.h>

#define BM_PI 3.141582

// VID and PID for Kinect and motor/acc/leds
#define MS_MAGIC_VENDOR 0x45e
#define MS_MAGIC_MOTOR_PRODUCT 0x02b0
// Constants for accelerometers
#define GRAVITY 9.80665
#define FREENECT_COUNTS_PER_G 819.
// The kinect can tilt from +31 to -31 degrees in what looks like 1 degree increments
// The control input looks like 2*desired_degrees
#define MAX_TILT_ANGLE 31.
#define MIN_TILT_ANGLE (-31.)

/// Enumeration of LED states
/// See http://openkinect.org/wiki/Protocol_Documentation#Setting_LED for more information.
typedef enum {
	LED_OFF              = 0, /**< Turn LED off */
	LED_GREEN            = 1, /**< Turn LED to Green */
	LED_RED              = 2, /**< Turn LED to Red */
	LED_YELLOW           = 3, /**< Turn LED to Yellow */
	LED_BLINK_GREEN      = 4, /**< Make LED blink Green */
	// 5 is same as 4, LED blink Green
	LED_BLINK_RED_YELLOW = 6, /**< Make LED blink Red/Yellow */
} freenect_led_options;

/// Enumeration of tilt motor status
typedef enum {
	TILT_STATUS_STOPPED = 0x00, /**< Tilt motor is stopped */
	TILT_STATUS_LIMIT   = 0x01, /**< Tilt motor has reached movement limit */
	TILT_STATUS_MOVING  = 0x04, /**< Tilt motor is currently moving to new position */
} freenect_tilt_status_code;

struct BM_KinectAuxState {
	int accelerometer_x;
	int accelerometer_y;
	int accelerometer_z;
	int tilt_angle;
	int tilt_status;
};

// init Kinect
int BM_KinectAux_init(int deviceIndex);
// shutdown Kinect
void BM_KinectAux_close();

// open USB connection
int BM_KinectAux_openDevice(int index);
// get kinect status
void BM_KinectAux_getState(struct BM_KinectAuxState* state);
// set tilt angle
void BM_KinectAux_setKinectTiltAngle(const double angleTarget);
// set LED color
void BM_KinectAux_setLedOption(const freenect_led_options option);

// get G vector of length 1
void BM_KinectAux_getGVector(XnVector3D *g);
// get device orientation in spherical coordinates [RAD]
void BM_KinectAux_getOrientation(double *roll_z, double *pitch_x, double *acc_y);
// Christoph's implementation
void CB_KinectAux_getOrientation(double *alpha, double *beta, double *gamma);

/**
 * Get the axis-based gravity adjusted accelerometer state, as laid
 * out via the accelerometer data sheet, which is available at
 *
 * http://www.kionix.com/Product%20Sheets/KXSD9%20Product%20Brief.pdf
 *
 * @param state State to extract accelerometer data from
 * @param x Stores X-axis accelerometer state
 * @param y Stores Y-axis accelerometer state
 * @param z Stores Z-axis accelerometer state
 */
void freenect_get_mks_accel(struct BM_KinectAuxState *state, double* x, double* y, double* z);

// shake your boody :-)
void BM_KinectAux_shake(int target);

#endif /* _KINECT_AUX_ */
