/*
 * ViewpointTracker
 *
 *  Created on: Nov 29, 2011
 *      Author: miggeb@ethz.ch
 */



#include "ViewpointTracker.h"
#include <XnTypes.h>

using namespace xn;

// ----------------
// Class
// ----------------
ViewpointTracker* ViewpointTracker::m_pInstance = NULL;

ViewpointTracker::ViewpointTracker() {
	nRetVal = XN_STATUS_OK;
	bPause = FALSE;
	g_bCalibrated = FALSE;
	g_nPlayer = 0;
	nUsers = 0;

	pthread_mutex_init(&repos_mutex, NULL);
	this->setSmoothingFactor(1.0f);
	this->initRepository();
	this->initKinect();

	// start thread
	pthread_create(&updateThread, NULL, &ViewpointTracker::updateLoop,
			(void *) NULL);
}

ViewpointTracker::~ViewpointTracker()
{
	bPause = TRUE;
	//pthread_join(updateThread);
	shutdownKinect();
	pthread_mutex_destroy(&repos_mutex);
}

// singleton
ViewpointTracker* ViewpointTracker::Instance()
{
	if (!m_pInstance) // Only allow one instance of class to be generated.
		m_pInstance = new ViewpointTracker;

	return m_pInstance;
}
// connect kinect, register callbacks
int ViewpointTracker::initKinect() {

	XnStatus rc = XN_STATUS_OK;
	xn::EnumerationErrors errors;

	// init kinect sensor
	rc = g_Context.InitFromXmlFile(XML_CONFIG_PATH, g_ScriptNode, &errors);
	CHECK_ERRORS(rc, errors, "InitFromXmlFile");
	CHECK_RC(rc, "InitFromXml");

	rc = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
	CHECK_RC(rc, "Find depth generator");
	rc = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
	CHECK_RC(rc, "Find user generator");

	rc = g_Context.StartGeneratingAll();
	CHECK_RC(rc, "StartGenerating");

	// register callbacks
	XnCallbackHandle hUserCBs;
	g_UserGenerator.RegisterUserCallbacks(NewUser, LostUser, NULL, hUserCBs);

	// init kinectAux
	BM_KinectAux_openDevice(0);
	BM_KinectAux_setLedOption(LED_OFF);
	BM_KinectAux_setKinectTiltAngle(0);

	return 0;
}

int ViewpointTracker::shutdownKinect() {
	g_ScriptNode.Release();
	g_DepthGenerator.Release();
	g_UserGenerator.Release();
	g_Context.Release();

	return 0;
}

int ViewpointTracker::refreshKinect() {
	xn::SceneMetaData sceneMD;
	xn::DepthMetaData depthMD;

	// Read next available data
	nRetVal = g_Context.WaitAndUpdateAll();
	if (nRetVal != XN_STATUS_OK)
	{
		printf("UpdateData failed: %s\n", xnGetStatusString(nRetVal));
		return -1;
	}

	// Process the data
	g_DepthGenerator.GetMetaData(depthMD);
	g_UserGenerator.GetUserPixels(0, sceneMD);
	updateRepository(depthMD, sceneMD);

	return 0;
}

// kinect control
void ViewpointTracker::setKinectTiltAngle(int angle)
{
	BM_KinectAux_setKinectTiltAngle(angle);
}


void ViewpointTracker::getKinectOrientation(double *roll_z, double *pitch_x, double *acc_y)
{
	BM_KinectAux_getOrientation(roll_z,pitch_x,acc_y);
}

void ViewpointTracker::initRepository()
{
	for(int userIndex = 0; userIndex < N_USERS; ++userIndex)
	{
		viewpoint_repos[userIndex].X = 0.0;
		viewpoint_repos[userIndex].Y = 0.0;
		viewpoint_repos[userIndex].Z = 0.0;
	}
}
// extract the head position from user tracker and depth camera
// VP = head top - VIEWPOINT_HEADTOP_OFFSET
void ViewpointTracker::updateRepository(const xn::DepthMetaData& depthMD,
		const xn::SceneMetaData& sceneMD) {

	//Read in the MetaData
	const XnLabel* pLabelsRow = sceneMD.Data();
	const XnDepthPixel* pDepthRow = depthMD.Data();

	bool bUserDetected[N_USERS];
	int nUsersDetected = 0;
	XnPoint3D projectionPoint, worldPoint; // tranformation helper

	XnVector3D gravity;
	double gravityLength;

	// setup normalized gravity vector
	BM_KinectAux_getGVector(&gravity);
	gravityLength = sqrt(pow(gravity.X,2) + pow(gravity.Y,2) + pow(gravity.Z,2));
	gravity.X = gravity.X / gravityLength;
	gravity.Y = gravity.Y / gravityLength;
	gravity.Z = gravity.Z / gravityLength;

	for(int userIndex = 0; userIndex < N_USERS; ++userIndex)
	{
		bUserDetected[userIndex] = FALSE;
	}

	// kinect orientation
	XnVector3D viewpointTranslation;

	// clear and update repository
	pthread_mutex_lock(&repos_mutex);

	float alpha = getSmoothingFactor();

	// get head position from userID map and depth map
	for (XnUInt y = 0; y < depthMD.YRes(); ++y) {
		const XnLabel* pLabels = pLabelsRow;
		const XnDepthPixel* pDepth = pDepthRow;

		for (XnUInt x = 0; x < depthMD.XRes(); ++x, ++pLabels, ++pDepth) {
			if (*pLabels != 0 && bUserDetected[*pLabels] == FALSE) //user found and not already detected
			{
				// update repository

				// head top position - viewpointTranslation
				int viewPointIndex = (*pLabels)-1;

				// convert data to real world coordinates: depthmap-> p1->p2
				projectionPoint.X = x; projectionPoint.Y = y; projectionPoint.Z = *pDepth;
				g_DepthGenerator.ConvertProjectiveToRealWorld(1,&projectionPoint,&worldPoint);

				// translate viewpoint about VIEWPOINT_HEADTOP_OFFSET in gravity direction
				// with exponential smooth with weight alpha
				viewpoint_repos[viewPointIndex].X = (1.0 - alpha) * viewpoint_repos[viewPointIndex].X +
						alpha * (worldPoint.X - gravity.X * VIEWPOINT_HEADTOP_OFFSET);
				viewpoint_repos[viewPointIndex].Y = (1.0 - alpha) * viewpoint_repos[viewPointIndex].Y +
										alpha * (worldPoint.Y - gravity.Y * VIEWPOINT_HEADTOP_OFFSET);
				viewpoint_repos[viewPointIndex].Z = (1.0 - alpha) * viewpoint_repos[viewPointIndex].Z +
										alpha * (worldPoint.Z - gravity.Z * VIEWPOINT_HEADTOP_OFFSET);


				// debug stuff
				//printf("VP: %f\n",viewpoint_repos[viewPointIndex].Y);
				//printf("HEAD: %f\n",worldPoint.Y );

				bUserDetected[*pLabels] = TRUE;
				++nUsersDetected;
			}
		}
		pLabelsRow += depthMD.XRes();
		pDepthRow += depthMD.XRes();

	}

	pthread_mutex_unlock(&repos_mutex);

	// visual at kinect
	if (nUsersDetected > 0) {
		BM_KinectAux_setLedOption(LED_BLINK_GREEN);
	} else {
		BM_KinectAux_setLedOption(LED_RED);
	}
}

// ---------------------------------------------
// CALLBACKS
// ---------------------------------------------
XnBool ViewpointTracker::AssignPlayer(XnUserID user) {
	if (g_nPlayer != 0)
		return FALSE;

	XnPoint3D com;
	g_UserGenerator.GetCoM(user, com);
	if (com.Z == 0)
		return FALSE;

	printf("Matching for existing calibration\n");
	g_UserGenerator.GetSkeletonCap().LoadCalibrationData(user, 0);
	g_UserGenerator.GetSkeletonCap().StartTracking(user);
	g_nPlayer = user;
	return TRUE;
}

int ViewpointTracker::setSmoothingFactor(double alpha)
{
	if (alpha < 0.0 || alpha > 1.0)
	{
		printf("value out of bounds: %d", alpha);
		return -1;
	}
	this->smoothingFactor = alpha;
	return 0;
}

double ViewpointTracker::getSmoothingFactor()
{
	return this->smoothingFactor;
}


///////// CALLBACKS ////////////

void XN_CALLBACK_TYPE ViewpointTracker::NewUser(xn::UserGenerator& generator,
		XnUserID user, void* pCookie) {
	printf("New user %d\n", user);
	(ViewpointTracker::m_pInstance->nUsers)++;
	//ViewpointTracker::nUsers++;
}

void XN_CALLBACK_TYPE ViewpointTracker::LostUser(xn::UserGenerator& generator,
		XnUserID user, void* pCookie) {
	printf("Lost user %d\n", user);
	(ViewpointTracker::m_pInstance->nUsers)--;
}

void ViewpointTracker::wipeRepository()
{
	pthread_mutex_lock(&repos_mutex);
	// wipe data structures
	for(int userIndex = 0; userIndex < N_USERS; ++userIndex)
	{
		viewpoint_repos[userIndex].X = NAN;
		viewpoint_repos[userIndex].Y = NAN;
		viewpoint_repos[userIndex].Z = NAN;
	}
	pthread_mutex_unlock(&repos_mutex);
}
void ViewpointTracker::printRepository()
{
	int userIndex;

	for(userIndex = 0; userIndex < N_USERS; ++userIndex)
	{
		printf("[%f,%f,%f] ",viewpoint_repos[userIndex].X, viewpoint_repos[userIndex].Y,
				viewpoint_repos[userIndex].Z);
	}
}

// copy repository
int ViewpointTracker::getViewpoints(XnVector3D* viewpointDestination,
		int n_viewpoints) {
	int n_copiedViewpoints, length;

	pthread_mutex_lock(&repos_mutex);
	n_copiedViewpoints = MIN(n_viewpoints, N_USERS);
	length = n_copiedViewpoints * sizeof(XnVector3D);
	memcpy(viewpointDestination, &viewpoint_repos, length);
	pthread_mutex_unlock(&repos_mutex);

	return n_copiedViewpoints;
}

int ViewpointTracker::getNUsers()
{
	return nUsers;
}

void* ViewpointTracker::updateLoop(void* args) {
	while (!m_pInstance->bPause)
	{
		m_pInstance->refreshKinect();
	}
}


