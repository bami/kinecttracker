/*
 * SkeletonTracker.cpp
 *
 *  Created on: Nov 21, 2011
 *      Author: bami
 */

#include "SkeletonTracker.h"
using namespace xn;

// ----------------
// Class
// ----------------
SkeletonTracker* SkeletonTracker::m_pInstance = NULL;

SkeletonTracker::SkeletonTracker() {
	nRetVal = XN_STATUS_OK;
	bPause = FALSE;
	g_bCalibrated = FALSE;
	g_nPlayer = 0;

	pthread_mutex_init(&repos_mutex, NULL);
	n_skeleton_repos = 0;

	initKinect();
	// start thread
	pthread_create(&updateThread, NULL, &SkeletonTracker::updateLoop,
			(void *) NULL);
}

SkeletonTracker::~SkeletonTracker() {
	bPause = TRUE;
	//pthread_join(updateThread);
	shutdownKinect();
	pthread_mutex_destroy(&repos_mutex);
}

// singleton
SkeletonTracker* SkeletonTracker::Instance() {
	if (!m_pInstance) // Only allow one instance of class to be generated.
		m_pInstance = new SkeletonTracker;

	return m_pInstance;
}
// connect kinect, register callbacks
XnStatus SkeletonTracker::initKinect()
{
	XnStatus rc = XN_STATUS_OK;
	xn::EnumerationErrors errors;

	// init kinect sensor
	rc = g_Context.InitFromXmlFile(XML_CONFIG_PATH, g_ScriptNode, &errors);
	CHECK_ERRORS(rc, errors, "InitFromXmlFile");
	CHECK_RC(rc, "InitFromXml");

	rc = g_Context.FindExistingNode(XN_NODE_TYPE_DEPTH, g_DepthGenerator);
	CHECK_RC(rc, "Find depth generator");
	rc = g_Context.FindExistingNode(XN_NODE_TYPE_USER, g_UserGenerator);
	CHECK_RC(rc, "Find user generator");

	if (!g_UserGenerator.IsCapabilitySupported(XN_CAPABILITY_SKELETON)
			|| !g_UserGenerator.IsCapabilitySupported(
					XN_CAPABILITY_POSE_DETECTION)) {
		printf(
				"User generator doesn't support either skeleton or pose detection.\n");
		return XN_STATUS_ERROR;
	}

	g_UserGenerator.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_ALL);

	rc = g_Context.StartGeneratingAll();
	CHECK_RC(rc, "StartGenerating");

	// register callbacks
	XnCallbackHandle hUserCBs, hCalibrationStartCB, hCalibrationCompleteCB,
			hPoseCBs;
	g_UserGenerator.RegisterUserCallbacks(NewUser, LostUser, NULL, hUserCBs);
	rc = g_UserGenerator.GetSkeletonCap().RegisterToCalibrationStart(
			CalibrationStarted, NULL, hCalibrationStartCB);
	CHECK_RC(rc, "Register to calbiration start");
	rc = g_UserGenerator.GetSkeletonCap().RegisterToCalibrationComplete(
			CalibrationCompleted, NULL, hCalibrationCompleteCB);
	CHECK_RC(rc, "Register to calibration complete");
	rc = g_UserGenerator.GetPoseDetectionCap().RegisterToPoseDetected(
			PoseDetected, NULL, hPoseCBs);
	CHECK_RC(rc, "Register to pose detected");

	// init kinectAux
	BM_KinectAux_openDevice(0);
	BM_KinectAux_setLedOption(LED_OFF);
	BM_KinectAux_setKinectTiltAngle(0);

	return rc;
}

void SkeletonTracker::shutdownKinect()
{
	bPause = TRUE;
	g_ScriptNode.Release();
	g_DepthGenerator.Release();
	g_UserGenerator.Release();
	g_Context.Release();
}

void SkeletonTracker::refreshKinect()
{
	// Read next available data from user generator only
	nRetVal = g_Context.WaitOneUpdateAll(g_UserGenerator);
	if (nRetVal != XN_STATUS_OK)
	{
		printf("UpdateData failed: %s\n", xnGetStatusString(nRetVal));
	}
}

// kinect control
void SkeletonTracker::setKinectTiltAngle(int angle)
{
	BM_KinectAux_setKinectTiltAngle(angle);
}

void SkeletonTracker::getKinectOrientation(double *roll,double *pitch, double *acc)
{
	BM_KinectAux_getOrientation(roll,pitch,acc);
}

// Save calibration to file
void SkeletonTracker::saveCalibration(std::string filename) {
	XnUserID aUserIDs[20] = { 0 };
	XnUInt16 nUsers = 20;
	g_UserGenerator.GetUsers(aUserIDs, nUsers);
	for (int i = 0; i < nUsers; ++i) {
		// Find a user who is already calibrated
		if (g_UserGenerator.GetSkeletonCap().IsCalibrated(aUserIDs[i])) {
			// Save user's calibration to file
			g_UserGenerator.GetSkeletonCap().SaveCalibrationDataToFile(
					aUserIDs[i], filename.c_str());
			break;
		}
	}
}
// Load calibration from file
void SkeletonTracker::loadCalibration(std::string filename) {
	XnUserID aUserIDs[20] = { 0 };
	XnUInt16 nUsers = 20;
	g_UserGenerator.GetUsers(aUserIDs, nUsers);
	for (int i = 0; i < nUsers; ++i) {
		// Find a user who isn't calibrated or currently in pose
		if (g_UserGenerator.GetSkeletonCap().IsCalibrated(aUserIDs[i]))
			continue;
		if (g_UserGenerator.GetSkeletonCap().IsCalibrating(aUserIDs[i]))
			continue;

		// Load user's calibration from file
		XnStatus rc =
				g_UserGenerator.GetSkeletonCap().LoadCalibrationDataFromFile(
						aUserIDs[i], filename.c_str());
		if (rc == XN_STATUS_OK) {
			// Make sure state is coherent
			g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(aUserIDs[i]);
			g_UserGenerator.GetSkeletonCap().StartTracking(aUserIDs[i]);
		}
		break;
	}
}

// ---------------------------------------------
// CALLBACKS
// ---------------------------------------------
XnBool SkeletonTracker::AssignPlayer(XnUserID user) {
	if (g_nPlayer != 0)
		return FALSE;

	XnPoint3D com;
	g_UserGenerator.GetCoM(user, com);
	if (com.Z == 0)
		return FALSE;

	printf("Matching for existing calibration\n");
	g_UserGenerator.GetSkeletonCap().LoadCalibrationData(user, 0);
	g_UserGenerator.GetSkeletonCap().StartTracking(user);
	g_nPlayer = user;
	return TRUE;
}

void SkeletonTracker::FindPlayer() {
	if (g_nPlayer != 0) {
		return;
	}
	XnUserID aUsers[20];
	XnUInt16 nUsers = 20;
	g_UserGenerator.GetUsers(aUsers, nUsers);

	for (int i = 0; i < nUsers; ++i) {
		if (AssignPlayer(aUsers[i]))
			return;
	}
}
void SkeletonTracker::LostPlayer() {
	g_nPlayer = 0;
	FindPlayer();
}

///////// CALLBACKS ////////////

void XN_CALLBACK_TYPE SkeletonTracker::NewUser(xn::UserGenerator& generator,
		XnUserID user, void* pCookie) {

	if (!m_pInstance->g_bCalibrated) // check on player0 is enough
	{
		printf("Look for pose\n");
		m_pInstance->g_UserGenerator.GetPoseDetectionCap().StartPoseDetection(
				"Psi", user);
		return;
	}

	m_pInstance->AssignPlayer(user);
	// 	if (g_nPlayer == 0)
	// 	{
	// 		printf("Assigned user\n");
	// 		g_UserGenerator.GetSkeletonCap().LoadCalibrationData(user, 0);
	// 		g_UserGenerator.GetSkeletonCap().StartTracking(user);
	// 		g_nPlayer = user;
	// 	}
}

void XN_CALLBACK_TYPE SkeletonTracker::LostUser(xn::UserGenerator& generator,
		XnUserID user, void* pCookie) {
	printf("Lost user %d\n", user);
	if (m_pInstance->g_nPlayer == user) {
		m_pInstance->LostPlayer();
	}
}
void XN_CALLBACK_TYPE SkeletonTracker::PoseDetected(
		xn::PoseDetectionCapability& pose, const XnChar* strPose,
		XnUserID user, void* cxt) {
	printf("Found pose \"%s\" for user %d\n", strPose, user);
	m_pInstance->g_UserGenerator.GetSkeletonCap().RequestCalibration(user, TRUE);
	m_pInstance->g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(user);
}

void XN_CALLBACK_TYPE SkeletonTracker::CalibrationStarted(
		xn::SkeletonCapability& skeleton, XnUserID user, void* cxt) {
	printf("Calibration started\n");
	BM_KinectAux_setLedOption(LED_BLINK_RED_YELLOW);
}

void XN_CALLBACK_TYPE SkeletonTracker::CalibrationEnded(
		xn::SkeletonCapability& skeleton, XnUserID user, XnBool bSuccess,
		void* cxt) {
	printf("Calibration done [%d] %ssuccessfully\n", user, bSuccess ? "" : "un");
	if (bSuccess) {
		if (!m_pInstance->g_bCalibrated) {
			m_pInstance->g_UserGenerator.GetSkeletonCap().SaveCalibrationData(
					user, 0);
			m_pInstance->g_nPlayer = user;
			m_pInstance->g_UserGenerator.GetSkeletonCap().StartTracking(user);
			m_pInstance->g_bCalibrated = TRUE;
		}

		XnUserID aUsers[10];
		XnUInt16 nUsers = 10;
		m_pInstance->g_UserGenerator.GetUsers(aUsers, nUsers);
		for (int i = 0; i < nUsers; ++i)
			m_pInstance->m_pInstance->g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(
					aUsers[i]);
	}
}

void XN_CALLBACK_TYPE SkeletonTracker::CalibrationCompleted(
		xn::SkeletonCapability& skeleton, XnUserID user,
		XnCalibrationStatus eStatus, void* cxt) {
	printf("Calibration done [%d] %ssuccessfully\n", user, (eStatus
			== XN_CALIBRATION_STATUS_OK) ? "" : "un");
	if (eStatus == XN_CALIBRATION_STATUS_OK) {
		if (!m_pInstance->g_bCalibrated) {
			m_pInstance->g_UserGenerator.GetSkeletonCap().SaveCalibrationData(
					user, 0);
			m_pInstance->g_nPlayer = user;
			m_pInstance->g_UserGenerator.GetSkeletonCap().StartTracking(user);
			m_pInstance->g_bCalibrated = TRUE;
		}

		XnUserID aUsers[10];
		XnUInt16 nUsers = 10;
		m_pInstance->g_UserGenerator.GetUsers(aUsers, nUsers);
		for (int i = 0; i < nUsers; ++i)
			m_pInstance->g_UserGenerator.GetPoseDetectionCap().StopPoseDetection(
					aUsers[i]);

		BM_KinectAux_setLedOption(LED_BLINK_GREEN);
	}
}

// get tracked skeletons and write to local skeleton repository
void XN_CALLBACK_TYPE SkeletonTracker::updateRepository() {
	XnUserID userIds[N_USERS];
	XnSkeletonJointPosition joint;
	int jointIndex, userIndex;
	XnUInt16 nUsers = N_USERS;

	g_UserGenerator.GetUsers(userIds, nUsers);

	pthread_mutex_lock(&repos_mutex);
	n_skeleton_repos = 0;

	for (userIndex = 0; userIndex < N_USERS; ++userIndex) {
		if (g_UserGenerator.GetSkeletonCap().IsTracking(userIds[userIndex])) {
			skeletons_id[userIndex] = userIds[userIndex];

			for (jointIndex = 0; jointIndex < N_JOINTS; ++jointIndex) {
				g_UserGenerator.GetSkeletonCap().GetSkeletonJointPosition(
						userIds[userIndex], XnSkeletonJoint(jointIndex + 1),
						joint);

				skeletons_repos[n_skeleton_repos][jointIndex].position
						= joint.position;
				skeletons_repos[n_skeleton_repos][jointIndex].fConfidence
						= joint.fConfidence;
			}
			skeletons_id[n_skeleton_repos] = userIds[userIndex];
			++n_skeleton_repos;
		}
	}
	pthread_mutex_unlock(&repos_mutex);

	if (n_skeleton_repos > 0) {
		BM_KinectAux_setLedOption(LED_BLINK_GREEN);
	} else {
		BM_KinectAux_setLedOption(LED_RED);
	}
}

void SkeletonTracker::wipeRepository()
{
	XnVector3D position;
	XnConfidence confidence;
	int userIndex, jointIndex;

	position.X = 0.0;
	position.Y = 0.0;
	position.Z = 0.0;
	confidence = 0.0;

	pthread_mutex_lock(&repos_mutex);
	for (userIndex = 0; userIndex < N_USERS; ++userIndex)
	{
		for (jointIndex = 0; jointIndex < N_JOINTS; ++jointIndex) {
			skeletons_repos[userIndex][jointIndex].position = position;
			skeletons_repos[userIndex][jointIndex].fConfidence = confidence;
		}
	}
	pthread_mutex_unlock(&repos_mutex);
}
void SkeletonTracker::printRepository()
{
	int userIndex, jointIndex;

	pthread_mutex_lock(&repos_mutex);
	for (userIndex = 0; userIndex < n_skeleton_repos; ++userIndex)
	{
		printf("User %d : ",userIndex);
		for (jointIndex = 0; jointIndex < N_JOINTS; ++jointIndex) {
			XnVector3D position = skeletons_repos[userIndex][jointIndex].position;
			XnConfidence confidence = skeletons_repos[userIndex][jointIndex].fConfidence;

			printf("[%f,%f,%f][%f] ", position.X, position.Y,position.Z,confidence);
		}
		printf("\n");
	}
	pthread_mutex_unlock(&repos_mutex);
}

// copy repository
int SkeletonTracker::getSkeletons(XnSkeletonJointPosition* skeletonsDestination,
		int n_Skeletons) {
	int n_copiedSkeletons, length;

	if (n_skeleton_repos < 1)
	{
		printf("No Skeletons identified.\n");
	}

	pthread_mutex_lock(&repos_mutex);
	n_copiedSkeletons = MIN(n_Skeletons, n_skeleton_repos);
	length = n_copiedSkeletons * N_JOINTS * sizeof(XnSkeletonJointPosition);
	memcpy(skeletonsDestination, &skeletons_repos, length);
	pthread_mutex_unlock(&repos_mutex);

	return n_copiedSkeletons;
}

void* SkeletonTracker::updateLoop(void* args) {
	while (!m_pInstance->bPause)
	{
		m_pInstance->refreshKinect();
		m_pInstance->updateRepository();
	}
}
