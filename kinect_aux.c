/*
 * kinect_aux.c
 *
 *  Created on: May 13, 2011
 *  Author: Bastian migge <miggeb@ethz.ch>, Christoph Bubenhofer
 *
 *  Based on kinect_aux http://www.ros.org/wiki/kinect
 */

#include "kinect_aux.h"

// --- DEFINES ---
#define BM_SMOOTH_ACC	10 //for smoothing the accelerometer data

// -- GLOBALS ---
libusb_device_handle *dev;

// --- CODE ---
int BM_KinectAux_openDevice(int index)
{
	libusb_device **devs; //pointer to pointer of device, used to retrieve a list of devices
	ssize_t cnt = libusb_get_device_list (0, &devs); //get the list of devices
	if (cnt < 0)
	{
		printf("No device on USB");
		return -1;
	}
	
	int nr_mot = 0;
	int i;
	for (i = 0; i < cnt; ++i)
	{
		struct libusb_device_descriptor desc;
		const int r = libusb_get_device_descriptor (devs[i], &desc);
		if (r < 0)
			continue;

		// Search for the aux
		if (desc.idVendor == MS_MAGIC_VENDOR && desc.idProduct == MS_MAGIC_MOTOR_PRODUCT)
		{
			// If the index given by the user matches our camera index
			if (nr_mot == index)
			{
				if ((libusb_open (devs[i], &dev) != 0) || (dev == 0))
				{
					printf("Cannot open aux %i", index);
					return;
				}
				// Claim the aux
				libusb_claim_interface (dev, 0);
				break;
			}
			else
				nr_mot++;
		}
	}

	libusb_free_device_list (devs, 1);  // free the list, unref the devices in it
	return 0;
}


void ST_KinectAux_getState(struct BM_KinectAuxState* state)
{
	uint8_t buf[10];
	const int ret = libusb_control_transfer(dev, 0xC0, 0x32, 0x0, 0x0, buf, 10, 0);
	if (ret != 10)
	{
		printf("Error in accelerometer reading, libusb_control_transfer returned %i",ret);
		return;
	}
	
	// extract data
	const uint16_t ux = ((uint16_t)buf[2] << 8) | buf[3];
	const uint16_t uy = ((uint16_t)buf[4] << 8) | buf[5];
	const uint16_t uz = ((uint16_t)buf[6] << 8) | buf[7];
	
	const int16_t accelerometer_x = (int16_t)ux;
	const int16_t accelerometer_y = (int16_t)uy;
	const int16_t accelerometer_z = (int16_t)uz;
	const int8_t tilt_angle = (int8_t)buf[8];
	const int tilt_status = buf[9];
	
	// pack data
	state->accelerometer_x = accelerometer_x;
	state->accelerometer_y = accelerometer_y;
	state->accelerometer_z = accelerometer_z;
	state->tilt_angle = tilt_angle;
	state->tilt_status = tilt_status;
}


void BM_KinectAux_setKinectTiltAngle(const double angleTarget)
{
	uint8_t empty[0x1];
	double angle;
	angle = (angleTarget<MIN_TILT_ANGLE) ? MIN_TILT_ANGLE : ((angleTarget>MAX_TILT_ANGLE) ? MAX_TILT_ANGLE : angleTarget);
	angle = angle * 2;
	const int ret = libusb_control_transfer(dev, 0x40, 0x31, (uint16_t)angle, 0x0, empty, 0x0, 0);
	if (ret != 0)
	{
		printf("Error in setting tilt angle, libusb_control_transfer returned %i",  ret);
		return;
	}
}

void BM_KinectAux_setLedOption(const freenect_led_options option)
{
	uint8_t empty[0x1];
	
	const int ret = libusb_control_transfer(dev, 0x40, 0x06, (uint16_t)option, 0x0, empty, 0x0, 0);
	if (ret != 0)
	{
		printf("Error in setting LED options, libusb_control_transfer returned %i",ret);
		return;
	}
}

// source from https://github.com/OpenKinect/libfreenect.git/fakenect/fakenect.c
void freenect_get_mks_accel(struct BM_KinectAuxState *state, double* x, double* y,
		double* z) {
	//the documentation for the accelerometer (http://www.kionix.com/Product%20Sheets/KXSD9%20Product%20Brief.pdf)
	//states there are 819 counts/g

	*x = (double) state->accelerometer_x / FREENECT_COUNTS_PER_G * GRAVITY;
	*y = (double) state->accelerometer_y / FREENECT_COUNTS_PER_G * GRAVITY;
	*z = (double) state->accelerometer_z / FREENECT_COUNTS_PER_G * GRAVITY;
}

int BM_KinectAux_init(int deviceIndex)
{
	int ret = libusb_init(0);
	if (ret)
	{
		return 1;
	}

	BM_KinectAux_openDevice(deviceIndex);

	if (!dev)
	{
		printf("No valid aux device found");
		libusb_exit(0);
		return 2;
	}
	return 0;
}

void BM_KinectAux_close()
{
	libusb_exit(0);
}

// get orientation in [RAD]
void BM_KinectAux_getGVector(XnVector3D *g)
{
        struct BM_KinectAuxState kstate;
        ST_KinectAux_getState(&kstate);
        double gX,gY,gZ;
        double length;
        // g-vector in kinect coordinates system
        gX = ((double)(kstate.accelerometer_x)/FREENECT_COUNTS_PER_G);
        gY = ((double)(kstate.accelerometer_y)/FREENECT_COUNTS_PER_G);
        gZ = ((double)(kstate.accelerometer_z)/FREENECT_COUNTS_PER_G);
        length = sqrt(gX*gX+gY*gY+gZ*gZ);
        gX /= length;
        gY /= length;
        gZ /= length;

        g->X = gX;
        g->Y = gY;
        g->Z = gZ;
}

// source is based on http://abstrakraft.org/cwiid/browser/wmgui/main.c
void BM_KinectAux_getOrientation(double *roll_z, double *pitch_x, double *acc_y)
{
	XnVector3D g;

	BM_KinectAux_getGVector(&g);

	*acc_y = sqrt(pow(g.X,2)+pow(g.Z,2)+pow(g.Y,2));

	*roll_z = atan(g.X/g.Y);
	if (g.Y <= 0.0)
	{
		*roll_z += BM_PI * ((g.X > 0.0) ? 1 : -1);
	}
	*roll_z *= -1;
	*pitch_x = atan(g.Z/g.Y*cos(*roll_z));
}

//Christoph's orientation calculation
void CB_KinectAux_getOrientation(double *alpha, double *beta, double *gamma)
{
	const int X_VALUE_MAX= 815; //max measured values for norming
	const int Y_VALUE_MAX= 759;
	const int Z_VALUE_MAX= 815;

	struct BM_KinectAuxState oneState;
	double accelerometerDataX=0;
	double accelerometerDataY=0;
	double accelerometerDataZ=0;
	int statecount;

	for (statecount=0; statecount < BM_SMOOTH_ACC; statecount++)
	{
		ST_KinectAux_getState(&oneState);
		accelerometerDataX+=oneState.accelerometer_x;
		accelerometerDataY+=oneState.accelerometer_y;
		accelerometerDataZ+=oneState.accelerometer_z;
	}
	accelerometerDataX=accelerometerDataX/(BM_SMOOTH_ACC*X_VALUE_MAX);		// divided through the Smoothing factor and the max_value for norming
	accelerometerDataY=accelerometerDataY/(BM_SMOOTH_ACC*Y_VALUE_MAX);
	accelerometerDataZ=accelerometerDataZ/(BM_SMOOTH_ACC*Z_VALUE_MAX);

	*gamma=-atan(accelerometerDataX/sqrt(pow(accelerometerDataZ,2)+pow(accelerometerDataY,2)));	//calculation of the right angles for correction to horizontal plane
	*beta=atan(accelerometerDataZ/sqrt(pow(accelerometerDataX,2)+pow(accelerometerDataY,2)));
	*alpha=asin(sin(*gamma)/cos(*beta));
}

void BM_KinectAux_shake(int target)
{
	BM_KinectAux_setKinectTiltAngle(MAX_TILT_ANGLE);
	sleep(3);
	BM_KinectAux_setKinectTiltAngle(MIN_TILT_ANGLE);
	sleep(3);
	BM_KinectAux_setKinectTiltAngle(target);
}
// example update loop - could be used for pthread.
int updateLoop(int argc, char* argv[])
{
	struct BM_KinectAuxState state;
	BM_KinectAux_init(0);

	while (1)
	{
		sleep(1);
		ST_KinectAux_getState(&state);
		printf("%d,%d,%d,%d,%d\n",state.accelerometer_x,state.accelerometer_y, \
				state.accelerometer_z,state.tilt_angle,state.tilt_status);
	}
	BM_KinectAux_close();
	return 0;
}
